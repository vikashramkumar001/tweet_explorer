Hi Chelseaaaa!

+ Insert the json_extraction.py into the same directory containing the all-rnr-annotated-threads directory
+ Insert the pheme_dataset.xlsx excel sheet into this directory as well

+ this is an example directory setup
- PHEME_veracity
    - all-rnr-annotated-threads
    - json_extraction.py
    - pheme_dataset.xlsx

+ run the script
    o use the VENV provided
        - command line
            > venv\Scripts\activate.bat
            > python json_extraction.py
    o it will take a while to run
        - my run took about 120 minutes - 6425 json files
        - you can tail the generated explorer.log to track progress
