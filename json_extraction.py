from openpyxl import Workbook, load_workbook
import os
import logging
import json
import time
import traceback

# setup basic logs to track what we are doing while we search for target json folders
logging_filename = os.path.join(os.getcwd(), 'explorer.log')
logging.basicConfig(
    filename=logging_filename,
    filemode='w+',
    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
    datefmt='%H:%M:%S',
    level=logging.DEBUG)

# excel variables

# excel sheet path
excel_source = os.path.join(os.getcwd(), 'pheme_dataset.xlsx')

# column structure to map headers to excel sheet
header_cols = {
    "ID": 1,
    "CONTENT": 2,
    "USER": 3,
    "DATE": 4,
    "LOCATION": 5,
    "TYPE": 6,
    "TOPIC": 7,
    "RETWEETS": 8
}

# starting row for excel sheet
current_row = 2

# target directory containing desired tweet jsons
target_directory = 'source-tweets'

# starting directory begin exploration
starting_directory = os.path.join(os.getcwd(), 'all-rnr-annotated-threads')

# simple count for tracking progress during extraction
count_files = 0


# extracts json data from json file and plugs into excel sheet
def extract(json_file):
    global current_row
    global count_files

    # logs
    logging.debug(str(count_files) + ' - ' + 'extracting - ' + json_file)

    # read file
    with open(json_file, 'r') as f:
        data = f.read()

    # parse file
    obj = json.loads(data)

    # write json data to excel

    # access excel file
    wb = load_workbook(filename=excel_source)
    ws = wb.active

    # write data to excel
    ws.cell(row=current_row, column=header_cols['ID']).value = str(obj['id'])
    ws.cell(row=current_row, column=header_cols['CONTENT']).value = str(obj['text'])
    ws.cell(row=current_row, column=header_cols['USER']).value = str(obj['user']['screen_name'])
    ws.cell(row=current_row, column=header_cols['DATE']).value = str(obj['created_at'])
    ws.cell(row=current_row, column=header_cols['LOCATION']).value = str(obj['coordinates'])
    ws.cell(row=current_row, column=header_cols['TYPE']).value = str(obj['favorite_count'])
    ws.cell(row=current_row, column=header_cols['TOPIC']).value = str(obj['truncated'])
    ws.cell(row=current_row, column=header_cols['RETWEETS']).value = str(obj['retweeted'])

    # increment global current row for next write
    current_row = current_row + 1
    count_files = count_files + 1

    # save work done to excel workbook
    wb.save(filename=excel_source)


# explores directory to find target directory
# sends files from target directory for extraction
def explorer(directory):

    # check all subdirectories
    for root, dirs, files in os.walk(directory):

        # isolate path with target directory
        if target_directory in root:

            # check if target directory is not empty
            if len(files) != 0:

                # get correct files from list of files

                # iterate through list of files in target directory
                for each_file in files:

                    # correct files format - does not start with '.' and ends with '.json'
                    if not each_file.startswith('.') and each_file.endswith('.json'):

                        # send json for extraction
                        extract(os.path.join(root, each_file))


if __name__ == '__main__':
    logging.info('Script initiated...')
    start_time = time.time()
    try:
        explorer(starting_directory)
        end_time_mins = (time.time() - start_time) / 60
        logging.debug('Script completed - ' + str(end_time_mins) + ' minutes')
    except Exception as e:
        end_time_mins = (time.time() - start_time) / 60
        logging.error("ERROR - " + str(e))
        logging.debug('Script halted - ' + str(end_time_mins) + ' minutes')
